import React, { useState, useEffect, ChangeEvent, KeyboardEvent, useRef } from 'react';
import './InputOTP.scss';

interface InputOTPProps {
    numInputs?: number;
    handleChange: (otp: string) => void;
}

const InputOTP: React.FC<InputOTPProps> = ({ numInputs = 6, handleChange }) => {
    const [otp, setOtp] = useState<string[]>(Array(numInputs).fill(''));
    const otpRefs = Array(numInputs)
        .fill(null)
        .map(() => useRef<HTMLInputElement>(null));

    useEffect(() => {
        if (otpRefs[0].current) {
            otpRefs[0].current.focus();
        }
    }, []);

    const handleChangeInput = (event: ChangeEvent<HTMLInputElement>, index: number) => {
        const fieldValue = /^[0-9]{1,1}$/.test(event.target.value) ? event.target.value : '';

        const otpFields = [...otp];
        otpFields[index] = fieldValue;
        setOtp(otpFields);

        handleChange(otpFields.join(''));

        if (fieldValue !== '') {
            const next = otpRefs[index].current?.nextElementSibling as HTMLInputElement;
            if (next && next.tagName === 'INPUT') {
                next.focus();
                setTimeout(() => next.select(), 0);
            }
        }
    };


    const handleKeyDown = (event: KeyboardEvent<HTMLInputElement>, index: number) => {
        if (event.key === 'Backspace' && event.currentTarget.value.length === 0) {
            const previous = otpRefs[index].current?.previousElementSibling as HTMLInputElement;
            if (previous && previous.tagName === 'INPUT') {
                previous.focus();
            }
        } else if (event.key === 'ArrowLeft') {
            const previous = otpRefs[index].current?.previousElementSibling as HTMLInputElement;
            if (previous && previous.tagName === 'INPUT') {
                previous.focus();
                setTimeout(() => previous.select(), 0);
            }
        } else if (event.key === 'ArrowRight') {
            const next = otpRefs[index].current?.nextElementSibling as HTMLInputElement;
            if (next && next.tagName === 'INPUT') {
                next.focus();
                setTimeout(() => next.select(), 0);
            }
        }
    };
    const handlePaste = (event: React.ClipboardEvent<HTMLInputElement>, index: number) => {
        event.preventDefault();
        const pastedText = event.clipboardData.getData('text/plain');

        if (/^\d{6}$/.test(pastedText)) {
            const otpFields = pastedText.split('').slice(0, numInputs);
            setOtp(otpFields);

            const nextIndex = Math.min(index + otpFields.length, numInputs - 1);
            const next = otpRefs[nextIndex].current as HTMLInputElement;
            if (next) {
                next.focus();
                setTimeout(() => next.select(), 0);
            }
        } else {
            otpRefs.forEach((ref) => {
                if (ref.current) {
                    ref.current.classList.add('shake-animation');
                    setTimeout(() => ref.current?.classList.remove('shake-animation'), 500);
                }
            });
        }
    };

    const isSubmitButtonDisabled = otp.join('').length !== numInputs;
    return (
        <div className="wrapper">
            <div className="heading">
                <h2>OTP Verification</h2>
                <p>Please enter the code we have sent you.</p>
            </div>
            <form>
                <div id="otp-container" >
                    {[...Array(numInputs)].map((_, index) => (
                        <input
                            type="text"
                            key={index}
                            name={`otp${index}`}
                            maxLength={1}
                            ref={otpRefs[index]}
                            onChange={(event) => handleChangeInput(event, index)}
                            onKeyDown={(event) => handleKeyDown(event, index)}
                            onPaste={(event) => handlePaste(event, index)}
                            value={otp[index]}
                            className="otp-number"
                        />
                    ))}
                </div>
                <button type='submit' value='Submit' disabled={isSubmitButtonDisabled}> Submit</button>
            </form>
        </div>
    );
};

export default InputOTP;
